import fs from "fs";
import $RefParser from "@apidevtools/json-schema-ref-parser";
import path from "path";

const dereferenceDir = async (dirPath: string, destPath: string) => {
  if (!fs.existsSync(destPath)) {
    fs.mkdirSync(destPath);
  }
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      return console.log("Unable to scan directory: " + err);
    }
    files.forEach(async (file) => {
      const originPath = path.join(dirPath, file);
      const derefPath = path.join(destPath, file);
      const stat = await fs.promises.stat(originPath);
      if (stat.isDirectory()) {
        if (file === "dereferenced") return;
        dereferenceDir(originPath, derefPath);
      }
      if (stat.isFile()) {
        $RefParser.dereference(originPath, (err, dereferenced) => {
          if (err) {
            return console.log("Unable to deref file: " + originPath + err);
          }
          fs.writeFileSync(path.join(derefPath), JSON.stringify(dereferenced));
        });
      }
    });
  });
};

const dirPath = path.join(__dirname, "../schemas");
const destPath = path.join(__dirname, "../schemas/dereferenced");

dereferenceDir(dirPath, destPath);
