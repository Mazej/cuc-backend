import prisma from "../../client";
import { Result } from "@badrap/result";
import type SeedFileStructure from "../../types/data-transfer-objects";
import { hashPassword } from "../../resources/user";

const seedDB = async (
  yamlParsed: SeedFileStructure,
  adminEmail: string,
  adminPassword: string
): Promise<Result<boolean>> => {
  try {
    prisma.$transaction([
      ...yamlParsed.role.map((single) => {
        return prisma.role.create({
          data: {
            ...single,
          },
        });
      }),

      ...yamlParsed.user.map((single) => {
        return prisma.user.create({
          data: {
            ...single,
          },
        });
      }),
      ...yamlParsed.userCredentials.map((single) => {
        return prisma.userCredentials.create({
          data: {
            ...single,
          },
        });
      }),
      ...yamlParsed.userNutrientsPerDay.map((single) => {
        return prisma.userNutrientsPerDay.create({
          data: {
            ...single,
          },
        });
      }),
      ...yamlParsed.userDetails.map((single) => {
        return prisma.userDetails.create({
          data: {
            ...single,
          },
        });
      }),

      ...yamlParsed.food.map((single) => {
        return prisma.food.create({
          data: {
            ...single,
          },
        });
      }),

      prisma.user.create({
        data: {
          credentials: {
            create: {
              email: adminEmail,
              passwordHash: await hashPassword(adminPassword),
            },
          },
          roleName: "admin",
        },
      }),
    ]);
    return Result.ok(true);
  } catch (e) {
    return Result.err(e as Error);
  }
};

export default seedDB;
