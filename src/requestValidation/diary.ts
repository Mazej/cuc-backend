import ajvInstance from "./ajvInstance";

import updateSchema from "../../schemas/dereferenced/diary/updateDiaryEntry.schema.json";
import createSchema from "../../schemas/dereferenced/diary/addToDiary.schema.json";

export const updateDiaryValidator = ajvInstance.compile(updateSchema);
export const addToDiaryValidator = ajvInstance.compile(createSchema);
