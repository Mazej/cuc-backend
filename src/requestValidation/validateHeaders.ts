import { NextFunction, Request, Response } from "express";
import ajvInstance from "./ajvInstance";
import jsend from "jsend";
import ajvErrorToResponseData from "./helper/ajvErrorToResponseData";

const headersSchema = {
  type: "object",
  properties: { authorization: { type: "string", format: "bearer-token" } },
  required: ["authorization"],
};

const headersValidate = ajvInstance.compile(headersSchema);

const validateHeaders = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    headersValidate(req.headers);
    headersValidate(req.headers);
    const data: any = {};
    if (headersValidate.errors) {
      data["headers"] = [];
      const errors = headersValidate.errors;
      errors.forEach((error) => {
        data.headers.push(ajvErrorToResponseData(error));
      });
      return res.status(401).send(jsend.fail(data));
    }
    return next();
  };
};

export default validateHeaders;
