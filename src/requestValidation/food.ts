import ajvInstance from "./ajvInstance";

import updateSchema from "../../schemas/dereferenced/food/foodProps.schema.json";
import createSchema from "../../schemas/dereferenced/food/createFood.schema.json";

export const updateFoodValidator = ajvInstance.compile(updateSchema);
export const createFoodValidator = ajvInstance.compile(createSchema);
