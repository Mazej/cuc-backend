import { ValidateFunction } from "ajv";
import { NextFunction, Request, Response } from "express";
import jsend from "jsend";
import ajvErrorToResponseData from "./helper/ajvErrorToResponseData";

const validateDto = (ajvValidate: ValidateFunction) => {
  return (req: Request, res: Response, next: NextFunction) => {
    ajvValidate(req.body);
    if (ajvValidate.errors) {
      const data: any = {};
      data.body = [];
      const errors = ajvValidate.errors;
      errors.forEach((error) => {
        data.body.push(ajvErrorToResponseData(error));
      });
      return res.status(400).send(jsend.fail(data));
    }
    return next();
  };
};
export default validateDto;
