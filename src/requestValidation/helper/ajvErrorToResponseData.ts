import { ErrorObject } from "ajv";

export default function ajvErrorToResponseData(error: ErrorObject) {
  return {
    instancePath: error.instancePath,
    keyword: error.keyword,
    params: error.params,
    message: error.message,
  };
}
