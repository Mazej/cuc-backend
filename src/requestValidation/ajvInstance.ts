import Ajv from "ajv";
import addFormats from "ajv-formats";

const ajv = new Ajv({ allErrors: true, strictRequired: true });
addFormats(ajv);
ajv.addFormat(
  "bearer-token",
  /Bearer\s[\d|a-f]{8}-([\d|a-f]{4}-){3}[\d|a-f]{12}/
);

export default ajv;
