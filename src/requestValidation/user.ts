import ajvInstance from "./ajvInstance";
import registrationSchema from "../../schemas/dereferenced/user/registerReq.schema.json";
import loginSchema from "../../schemas/dereferenced/user/loginReq.schema.json";
import updatePasswordSchema from "../../schemas/dereferenced/user/updatePasswordReq.schema.json";
import updateSchema from "../../schemas/dereferenced/user/updateReq.schema.json";

export const registrationValidator = ajvInstance.compile(registrationSchema);
export const loginValidator = ajvInstance.compile(loginSchema);
export const updatePasswordValidator =
  ajvInstance.compile(updatePasswordSchema);
export const updateUserValidator = ajvInstance.compile(updateSchema);
