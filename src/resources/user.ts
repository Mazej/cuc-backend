import { Request, Response } from "express";
import {
  sendAuthorizationError,
  sendCreatedSuccessfully,
  sendInternalServerError,
  sendSuccess,
} from "./helper/responses";
import prisma from "../client";
import { v4 as uuid } from "uuid";
import { UserDetails, User, UserNutrientsPerDay } from ".prisma/client";
import handleUsualErrors from "./helper/handleUsualErrors";
import { object, string } from "yup";
import bcrypt from "bcryptjs";

export const headersSchema = object({
  authorization: string()
    .required()
    .transform((val, _) => {
      return val.split(" ")[1] || val;
    })
    .test(
      "is valid bearer token",
      `Authorization header does not containt valid bearer token`,
      (val, _) => {
        return !!val;
      }
    ),
});

export const register = async (req: Request, res: Response) => {
  try {
    const data = req.body;

    const userId = uuid();

    const t: any = [
      prisma.user.create({
        data: {
          id: userId,
          details: {
            create: { ...data.details },
          },
          credentials: {
            create: {
              email: data.details.email,
              passwordHash: await hashPassword(data.password),
            },
          },
        },
      }),
    ];

    if (data.nutrientsPerDay) {
      t.push(
        prisma.userNutrientsPerDay.create({
          data: { ...data.nutrientsPerDay, userId: userId },
        })
      );
    }

    await prisma.$transaction(t);

    const user = await prisma.user.findUnique({
      where: { id: userId },
      include: { nutrientsPerDay: true, details: true, role: true },
      rejectOnNotFound: true,
    });

    const { id: sessionId } = await prisma.session.create({
      data: { userId: user.id },
    });

    return sendCreatedSuccessfully(res, {
      sessionId,
      role: user.role.name,
      user: addComputedAttributes(user),
    });
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );
    const data = req.body;

    const { id: userId } = await authenticateUser(sessionId);

    await prisma.$transaction(async (prisma) => {
      await prisma.userDetails.update({
        where: { userId: userId },
        data: { ...data.details },
      });

      if (data.nutrientsPerDay)
        await prisma.userNutrientsPerDay.upsert({
          where: { userId: userId },
          update: { ...data.nutrientsPerDay },
          create: {
            ...data.nutrientsPerDay,
            userId: userId,
          },
        });
      // TODO vyresit aby se nemusel email ukladat duplicitne
      if (data.details?.email)
        await prisma.userCredentials.update({
          where: { userId: userId },
          data: { email: data.details.email },
        });
    });

    const user = await prisma.user.findUnique({
      where: { id: userId },
      include: { nutrientsPerDay: true, details: true },
      rejectOnNotFound: true,
    });
    return sendSuccess(res, addComputedAttributes(user));
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const get = async (req: Request, res: Response) => {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    const { id } = await authenticateUser(sessionId);
    const user = await prisma.user.findUnique({
      where: { id },
      include: { nutrientsPerDay: true, details: true },
      rejectOnNotFound: true,
    });

    if (!user.nutrientsPerDay) {
      if (!user.details) return sendInternalServerError(res);
      user.nutrientsPerDay = {
        ...calculateNutrientsPerDay(user.details),
        userId: user.id,
      };
    }

    return sendSuccess(res, addComputedAttributes(user));
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const password = req.body.password;
    const email = req.body.email;

    const {
      userId,
      passwordHash: storedPasswordHash,
      user: {
        role: { name: roleName },
      },
    } = await prisma.userCredentials.findUnique({
      where: { email: email },
      include: {
        user: {
          include: { role: true },
        },
      },
      rejectOnNotFound: true,
    });

    if (!(await bcrypt.compare(password, storedPasswordHash)))
      return sendAuthorizationError(res, {
        message: "Email and password do not match",
      });

    const { id: sessionId } = await prisma.session.create({
      data: { userId: userId },
    });

    return sendSuccess(res, { sessionId, role: roleName });
  } catch (e: any) {
    if (e.name === "NotFoundError") {
      if (e.message === "No UserCredentials found")
        return sendAuthorizationError(res, {
          message: "Email and password do not match",
        });
    }

    handleUsualErrors(e, res);
  }
};

export const logout = async (req: Request, res: Response) => {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    await prisma.session.delete({ where: { id: sessionId } });

    return sendSuccess(res);
  } catch (e: any) {
    if (e.code === "P2025") return sendSuccess(res);

    handleUsualErrors(e, res);
  }
};

export async function updatePassword(req: Request, res: Response) {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;

    const { id: userId } = await authenticateUser(sessionId);

    const { passwordHash: storedPasswordHash } =
      await prisma.userCredentials.findUnique({
        where: { userId: userId },
        rejectOnNotFound: true,
      });

    if (!(await bcrypt.compare(oldPassword, storedPasswordHash)))
      return sendAuthorizationError(res, {
        message: "Old user password does not match",
      });

    await prisma.userCredentials.update({
      where: { userId: userId },
      data: { passwordHash: await hashPassword(newPassword) },
    });

    return sendSuccess(res);
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
}

export const deleteUser = async (req: Request, res: Response) => {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    const { id: userId } = await authenticateUser(sessionId);

    await prisma.user.delete({ where: { id: userId } });
    return sendSuccess(res);
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

const calculateNutrientsPerDay = ({
  yearOfBirth,
  weight,
  height,
  sex,
  goalWeight,
}: UserDetails) => {
  const age = new Date().getFullYear() - yearOfBirth;
  let calories: number =
    (10 * weight +
      6.25 * height -
      5 * age +
      (sex === "male" ? 1 : 0) * 166 -
      161) *
    1.5;

  if (goalWeight - weight > 7) calories += 750;
  else if (goalWeight - weight < -7) calories -= 750;
  else calories += ((goalWeight - weight) / 100) * 750;

  const protein = 2 * weight;
  const caloriesWoProtein = calories - protein * 4;

  return {
    protein: protein,
    carbs: Math.round((caloriesWoProtein / 4) * 0.7),
    fat: Math.round((caloriesWoProtein / 4) * 0.3),
    fiber: Math.round((weight / 100) * 40),
    salt: 2,
    deleted: null,
  };
};

export const authenticateUser = async (
  sessionId: string | undefined,
  getRole: boolean = false
) => {
  const session = await prisma.session.findUnique({
    where: { id: sessionId },
    include: {
      user: { select: { id: true, role: getRole } },
    },
    rejectOnNotFound: true,
  });
  return session.user;
};

const addComputedAttributes = (
  user: User & {
    nutrientsPerDay: UserNutrientsPerDay | null;
    details: UserDetails | null;
  }
) => {
  if (user.details === null) throw new Error("User details value is null");

  const nutrients =
    user.nutrientsPerDay || calculateNutrientsPerDay(user.details);
  const calories =
    nutrients?.carbs * 4 + nutrients?.protein * 4 + nutrients.fat * 9;
  return {
    ...user,
    nutrientsPerDay: {
      ...nutrients,
      calories,
    },
  };
};

export const hashPassword = async (password: string) => {
  const salt = await bcrypt.genSalt(12);
  return bcrypt.hash(password, salt);
};
