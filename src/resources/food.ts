import { string, number } from "yup";
import { Request, Response } from "express";
import prisma from "../client";
import {
  sendAuthorizationError,
  sendCreatedSuccessfully,
  sendSuccess,
} from "./helper/responses";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime";
import { headersSchema } from "./user";
import { authenticateUser } from "./user";
import { Role } from ".prisma/client";
import handleUsualErrors from "./helper/handleUsualErrors";
import { Food } from ".prisma/client";

const foodIdSchema = string().required().lowercase();

export const store = async (req: Request, res: Response) => {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    const user = await authenticateUser(sessionId, true);
    if (!user.role.canCUDAnyFood && !user.role.canCUDOwnFood)
      return sendAuthorizationError(res);

    const data = req.body;

    const food = await prisma.food.create({
      data: {
        ...data,
        lowercaseName: data.name.toLowerCase(),
        creatorId: user.id,
      },
    });
    return sendCreatedSuccessfully(res, addComputedCalories(food));
  } catch (e: any) {
    if (e instanceof PrismaClientKnownRequestError && e.code === "P2002")
      if (e.meta) e.meta["target"] = "name";

    handleUsualErrors(e, res);
  }
};

export const getAll = async (_: Request, res: Response) => {
  try {
    const food = await prisma.food.findMany({
      where: { deleted: null },
      orderBy: { name: "asc" },
    });

    const result = food.map((food) => {
      addComputedCalories(food);
    });

    return sendSuccess(res, result);
  } catch (e) {
    handleUsualErrors(e, res);
  }
};

export const getById = async (req: Request, res: Response) => {
  const id = req.params["id"]!;
  try {
    const food = await prisma.food.findUnique({
      where: { id: id },
      rejectOnNotFound: true,
    });

    return sendSuccess(res, addComputedCalories(food));
  } catch (e) {
    handleUsualErrors(e, res);
  }
};

export const get = async (req: Request, res: Response) => {
  try {
    const id = await foodIdSchema.validate(req.params["name"]);

    const food = await prisma.food.findFirst({
      where: {
        AND: [{ deleted: null }, { id: id }],
      },
      rejectOnNotFound: true,
    });

    return sendSuccess(res, addComputedCalories(food));
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const searchByName = async (req: Request, res: Response) => {
  try {
    const partOfId = req.params["name"]!.toLowerCase();

    const food =
      partOfId === '""'
        ? await prisma.food.findMany({ orderBy: { name: "asc" } })
        : await prisma.food.findMany({
            where: {
              AND: [
                { deleted: null },
                { lowercaseName: { contains: partOfId } },
              ],
            },
            orderBy: { name: "asc" },
          });

    const result = food.map((food) => {
      return addComputedCalories(food);
    });

    return sendSuccess(res, result);
  } catch (e) {
    handleUsualErrors(e, res);
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );
    const user = await authenticateUser(sessionId, true);

    const foodId = await foodIdSchema.validate(req.params["name"]);

    if (!(await canUserUpdateAndDeleteFood(foodId, user.id, user.role)))
      return sendAuthorizationError(res);

    const data = req.body;

    if (data.sugar) {
      const carbs =
        data.carbs ||
        (
          await prisma.food.findUnique({
            where: { id: foodId },
            select: { carbs: true },
            rejectOnNotFound: true,
          })
        ).carbs;
      number().min(0).max(carbs).validateSync(data.sugar);
    }

    await prisma.food.update({
      where: { id: foodId },
      data: { ...data, lowercaseName: data.name?.toLowerCase() },
    });

    const updatedFood = await prisma.food.findUnique({
      where: { id: foodId },
      rejectOnNotFound: true,
    });

    return sendSuccess(res, addComputedCalories(updatedFood));
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const deleteFood = async (req: Request, res: Response) => {
  try {
    const foodId = await foodIdSchema.validate(req.params["name"]);

    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );
    const user = await authenticateUser(sessionId, true);

    if (!(await canUserUpdateAndDeleteFood(foodId, user.id, user.role)))
      return sendAuthorizationError(res);

    await prisma.food.delete({
      where: { id: foodId },
    });
    return sendSuccess(res, {});
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

const addComputedCalories = (food: Food) => {
  return {
    ...food,
    calories: food.carbs * 4 + food.protein * 4 + food.fat * 9,
  };
};

const canUserUpdateAndDeleteFood = async (
  foodId: string,
  userId: string,
  userRole: Role
) => {
  if (userRole.canCUDAnyFood) return true;
  if (userRole.canCUDOwnFood) {
    const { creatorId } = await prisma.food.findUnique({
      where: { id: foodId },
      rejectOnNotFound: true,
    });
    return creatorId === userId;
  }
  return false;
};
