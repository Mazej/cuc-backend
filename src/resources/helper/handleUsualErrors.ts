import { PrismaClientKnownRequestError } from "@prisma/client/runtime";
import { Response } from "express";
import jsend from "jsend";
import { sendInternalServerError } from "./responses";

const handleUsualErrors = (e: any, res: Response) => {
  if (e instanceof PrismaClientKnownRequestError && e.code === "P2002") {
    if (e.meta === undefined) return sendInternalServerError(res);

    const invalidProperty = e.meta["target"];
    const data = {
      body: [
        {
          instancePath: "",
          keyword: "duplicity",
          params: { invalidProperty: invalidProperty },
          message: `Entity with same ${invalidProperty} already exists`,
        },
      ],
    };
    return res.status(409).send(jsend.fail(data));
  }

  if (e.name === "NotFoundError") {
    if (e.message === "No Session found") {
      const data = {
        headers: [
          {
            instancePath: "",
            keyword: "expired",
            params: { invalidProperty: "athorization" },
            message: "Expired authorization token",
          },
        ],
      };
      return res.status(401).send(jsend.fail(data));
    }
    const data = {
      keyword: "notFound",
      message: `Entity not found`,
    };
    return res.status(404).send(jsend.fail(data));
  }

  console.log(e);
  return sendInternalServerError(res);
};

export default handleUsualErrors;
