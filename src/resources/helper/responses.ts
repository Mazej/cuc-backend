import { Response } from "express";
import jsend from "jsend";

export const sendSuccess = (res: Response, data: any = {}) => {
  res.status(200).send(jsend.success(data));
};

export const sendCreatedSuccessfully = (res: Response, data: any) => {
  res.status(201).send(jsend.success(data));
};

export const sendAuthorizationError = (
  res: Response,
  data: object = {
    message: "User is not authorized to perform this type of action",
  }
) => {
  res.status(401).send(
    jsend.fail({
      data,
    })
  );
};

export const sendInternalServerError = (res: Response) => {
  return res.status(500).send(jsend.error("Internal server error"));
};
