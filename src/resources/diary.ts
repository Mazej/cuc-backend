import { Request, Response } from "express";
import prisma from "../client";
import {
  sendAuthorizationError,
  sendCreatedSuccessfully,
  sendSuccess,
} from "./helper/responses";
import { authenticateUser } from "./user";
import { headersSchema } from "./user";
import handleUsualErrors from "./helper/handleUsualErrors";

export const get = async (req: Request, res: Response) => {
  // NOTE date stored in the database in in TZ 0, but our date is in TZ +2
  try {
    const date = new Date(req.params["date"] || "");
    const nextDay = new Date(new Date(date).setDate(date.getDate() + 1));
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    const user = await authenticateUser(sessionId);

    const diary = await prisma.diaryEntry.findMany({
      where: {
        AND: [{ userId: user.id }, { date: { gte: date, lt: nextDay } }],
      },
      include: { food: true },
    });

    return sendSuccess(res, diary);
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const store = async (req: Request, res: Response) => {
  try {
    const data = req.body;

    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    const user = await authenticateUser(sessionId);

    // Test if food exists
    await prisma.food.findUnique({ where: { id: data.foodId } });

    const diaryEntry = await prisma.diaryEntry.create({
      data: { ...data, userId: user.id, date: new Date(data.date) },
      include: { food: true },
    });

    return sendCreatedSuccessfully(res, diaryEntry);
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    const data = req.body;

    // Test if food exists
    if (data.foodId)
      await prisma.food.findUnique({ where: { id: data.foodId } });

    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    await authenticateUser(sessionId);

    await prisma.diaryEntry.update({
      where: { id: data.id },
      data: { ...data, date: new Date(data.date) },
    });

    const diaryEntry = await prisma.diaryEntry.findUnique({
      where: { id: data.id },
    });

    return sendSuccess(res, diaryEntry);
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};

export const deleteEntry = async (req: Request, res: Response) => {
  try {
    const id = req.params["id"];
    const { authorization: sessionId } = await headersSchema.validate(
      req.headers
    );

    const { id: userId } = await authenticateUser(sessionId);

    const entry = await prisma.diaryEntry.findUnique({
      where: { id },
      rejectOnNotFound: true,
    });

    if (entry.userId !== userId)
      return sendAuthorizationError(res, {
        message: "User is not authorized to delete entry with given id",
      });

    await prisma.diaryEntry.delete({ where: { id } });

    return sendSuccess(res);
  } catch (e: any) {
    handleUsualErrors(e, res);
  }
};
