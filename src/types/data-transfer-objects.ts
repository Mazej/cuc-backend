export default interface SeedFileStructure {
  userCredentials: UserCredentialsDTO[];
  userDetails: UserDetailsDTO[];
  userNutrientsPerDay: UserNutrientsPerDayDTO[];
  food: FoodDTO[];
  eaten: EatenDTO[];
  user: UserDTO[];
  session: SessionDTO[];
  diaryEntry: DiaryEntryDTO[];
  role: RoleDTO[];
}

export interface UserDTO {
  id: string;
  roleName: string;
}

export interface UserCredentialsDTO {
  email: string;
  userId: string;
  passwordHash: string;
}

export interface UserDetailsDTO {
  weight: number;
  height: number;
  yearOfBirth: number;
  sex: string;
  email: string;
  userId: string;
  goalWeight: number;
}

export interface UserNutrientsPerDayDTO {
  id: string;
  userId: string;
  carbs: number;
  protein: number;
  fat: number;
  fiber: number;
  salt: number;
  calories: number;
}

export interface FoodDTO {
  id: string;
  name: string;
  lowercaseName: string;
  description: string;
  imageUrl: string;
  protein: number;
  carbs: number;
  sugar: number;
  fat: number;
  fiber: number;
  salt: number;
  creatorId: string;
}

export interface EatenDTO {
  id: string;
  foodId: string;
  quantity: number;
}

export interface SessionDTO {
  id: string;
  userId: string;
}

export interface DiaryEntryDTO {
  id: string;
  userId: string;
  foodId: string;
  date: Date;
  grams: number;
  mealType: string;
}

export interface RoleDTO {
  name: string;
  canDeleteUsers: boolean;
  canPromoteUsers: boolean;
  canCUDOwnFood: boolean;
  canCUDAnyFood: boolean;
}
