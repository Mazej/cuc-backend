import express from "express";
import { diary, food, user } from "./resources";
import swaggerUi from "swagger-ui-express";
import SwaggerParser from "@apidevtools/swagger-parser";
import helmet from "helmet";
import {
  registrationValidator,
  loginValidator,
  updateUserValidator,
  updatePasswordValidator,
} from "./requestValidation/user";
import validateBody from "./requestValidation/validateBody";
import {
  createFoodValidator,
  updateFoodValidator,
} from "./requestValidation/food";
import {
  addToDiaryValidator,
  updateDiaryValidator,
} from "./requestValidation/diary";
import validateHeaders from "./requestValidation/validateHeaders";

const api = express();
api.use([
  helmet(),
  express.json(),
  express.urlencoded({ extended: true }),
  express.static("public"),
]);

SwaggerParser.dereference("public/docs/openapi.yaml", (err, bundle) => {
  if (err) console.log("Error occurred while bundeling swagger file:\n", err);
  else {
    api.use("/api-docs", swaggerUi.serve, swaggerUi.setup(bundle));
  }
});

api.get("/", (_, res) =>
  res.send({
    status: "success",
    data: {},
    message: "Welcome to our API",
  })
);

// ENDPOINTS FOR FOOD //
api.post(
  "/api/food",
  validateHeaders(),
  validateBody(createFoodValidator),
  food.store
);
api.get("/api/food/search/:name", food.searchByName);
api.get("/api/food/search", food.getAll);
api.put(
  "/api/food/:name",
  validateHeaders(),
  validateBody(updateFoodValidator),
  food.update
);
api.get("/api/food/:name", food.get);
api.get("/api/food", food.getAll);

api.delete("/api/food/:name", food.deleteFood);

// GETTING INFORMATION ABOUT USER //
api.get("/api/user", user.get);

/* DIARY */
api.get("/api/diary/:date", diary.get);
api.post("/api/diary", validateBody(addToDiaryValidator), diary.store);
api.put("/api/diary", validateBody(updateDiaryValidator), diary.update);
api.delete("/api/diary/:id", diary.deleteEntry);

// ENDPOINTS FOR LOGIN AND REGISTRATION //
api.post("/api/register", validateBody(registrationValidator), user.register);
api.post("/api/login", validateBody(loginValidator), user.login);
api.post("/api/logout", user.logout);

// ENDPOINT FOR UPDATING USER DATA //
api.put("/api/user", validateBody(updateUserValidator), user.update);
api.put(
  "/api/user/password",
  validateBody(updatePasswordValidator),
  user.updatePassword
);

/* DELETING USER */
api.delete("/api/user", user.deleteUser);

const port = process.env["PORT"] || 3000;
api.listen(port, () => {
  console.log(`Express app is listening at http://localhost:${port}`);
});
