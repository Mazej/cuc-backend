<div id="top"></div>

[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/Mazej/cuc-backend">
    <img src="images/logo-background.png" alt="Logo" width="160" height="160">
  </a>

  <h3 align="center">Calories under control - backend</h3>

  <p align="center">
    The back end of an application for tracking personal macros.
    <br />
    <a href="https://gitlab.com/Mazej/cuc-backend"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://www.postman.com/dark-water-192541/workspace/caloric-tables-backend/collection/15925036-461fea41-eaec-484a-a558-db1d243ab064?action=share&creator=15925036">View Demo in Postman</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

I have been using a website called http://www.kaloricketabulky.cz (something similar to **MyFitnessPal**) for a couple of years. It allows users to search for different kinds of food, view its calories and nutrients. Users can also track food eaten during the day and then view informative graphs telling how much they filled the recommended values for macros.  

So I decided to create my own version of this website as a team project for one of my university courses, which aims to teach us about web development.

This is the backend part I created for this project.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

[![Ts][Ts-shield]][Ts-url]
[![Nodejs][Nodejs-shield]][Nodejs-url]
[![Prisma][Prisma-shield]][Prisma-url]
[![Express][Express-shield]][Express-url]
[![OpenApi][OpenApi-shield]][OpenApi-url]
[![Swagger][Swagger-shield]][Swagger-url]
[![SQLite][SQLite-shield]][SQLite-url]

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* Node.js http://nodejs.org
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/github_username/repo_name.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Initialize database - set admin email and admin password
   for Windows:
     ```sh
     set ADMIN_EMAIL="admin@example.com" && set ADMIN_PASSWORD="admin123" && npm run init 
     ```
   for Linux and Mac OS:
     ```sh
     export ADMIN_EMAIL="admin@example.com" && export ADMIN_PASSWORD="admin123" && npm run init 
     ```
4. Run the server
    ```sh
      npm start
    ```
   

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

### Managing the server

Run the server

  ```sh
    npm start
  ```
Reset the database

* for Windows:
  ```sh
  set ADMIN_EMAIL="admin@example.com" && set ADMIN_PASSWORD="admin123" && npm run init 
  ```
* for Linux and Mac OS:
  ```sh
  export ADMIN_EMAIL="admin@example.com" && export ADMIN_PASSWORD="admin123" && npm run init 
  ```

### Sending requests

  <details> <summary>Register user</summary>

  ```sh
  curl --location --request POST 'localhost:3000/api/register' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "password": "wfAgkQ7eLYc9v28",
      "details": {
          "height": 150,
          "weight": 231,
          "yearOfBirth": 1980,
          "sex": "male",
          "email": "Lorine_Botsford@yahoo.com",
          "goalWeight": 80
      },
      "nutrientsPerDay": {
          "protein": 150,
          "carbs": 230,
          "fat": 79,
          "fiber": 18,
          "salt": 5
      }
  }'
  ```
  </details>

  <details>
  <summary>Search for "banana"</summary>

  ```sh
curl --location --request GET 'localhost:3000/api/food/search/banana'
  ```
  </details>


  <details>
  <summary>List all food stored in the database</summary>

  ```sh
curl --location --request GET 'localhost:3000/api/food'
  ```
  </details>
&nbsp;


_For more examples, please refer to the [Examples in Postman][postman-demo-url] or the [Swagger documentation][swagger-documentation-url]_

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

Matěj Kubík - matelf.kubik@gmail.com

Project Link: [https://gitlab.com/Mazej/cuc-backend](https://gitlab.com/Mazej/cuc-backend)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* [Choose an Open Source License](https://choosealicense.com)
* [Best-README-Template](https://github.com/othneildrew/Best-README-Template)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/kubikmtj

[Nodejs-url]: https://nodejs.org/
[Nodejs-shield]: https://img.shields.io/badge/Node.js-93c541?style=for-the-badge&logo=node.js&logoColor=gray
[Ts-shield]: https://img.shields.io/badge/Typescript-007acc?style=for-the-badge&logo=typescript&logoColor=white
[Ts-url]: https://www.typescriptlang.org/
[Prisma-shield]: https://img.shields.io/badge/Prisma-EEF?style=for-the-badge&logo=prisma&logoColor=black
[Prisma-url]: https://www.prisma.io/
[Express-shield]: https://img.shields.io/badge/Express-EEE?style=for-the-badge&logo=Express&logoColor=black
[Express-url]: https://expressjs.com/
[OpenApi-shield]: https://img.shields.io/badge/OpenApi-1c1c3f?style=for-the-badge&logo=openapiinitiative&logoColor=#4c5d32
[OpenApi-url]:https://www.openapis.org/
[Swagger-shield]: https://img.shields.io/badge/Swagger-83eb2c?style=for-the-badge&logo=swagger&logoColor=black
[Swagger-url]: https://swagger.io/
[SQLite-shield]: https://img.shields.io/badge/SQLite-43a4d9?style=for-the-badge&logo=SQLite&logoColor=054a60
[SQLite-url]: https://www.sqlite.org/index.html

[postman-demo-url]: https://www.postman.com/dark-water-192541/workspace/caloric-tables-backend/collection/15925036-461fea41-eaec-484a-a558-db1d243ab064?ctx=documentation
[swagger-documentation-url]: https://caloric-tables.kubikm.xyz/api-docs/
